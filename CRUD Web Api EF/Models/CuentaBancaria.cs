﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace CRUD_Web_Api_EF.Models
{
    public partial class CuentaBancaria
    {
        public CuentaBancaria()
        {
            Transaccion = new HashSet<Transaccion>();
        }

        public int IdCuentaBancaria { get; set; }
        public int IdCliente { get; set; }
        public string Moneda { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual ICollection<Transaccion> Transaccion { get; set; }
    }
}
