﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace CRUD_Web_Api_EF.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            CuentaBancaria = new HashSet<CuentaBancaria>();
        }

        public int IdCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }

        public virtual ICollection<CuentaBancaria> CuentaBancaria { get; set; }
    }
}
