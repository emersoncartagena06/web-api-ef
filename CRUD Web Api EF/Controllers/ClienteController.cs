﻿using CRUD_Web_Api_EF.Models;
using Microsoft.AspNetCore.Mvc;

namespace CRUD_Web_Api_EF.Controllers
{
    [ApiController]
    [Route("API/[controller]")]
    [Produces("application/json")]
    public class ClienteController : ControllerBase
    {

        private readonly bancoContext context;

        public ClienteController(bancoContext _context)
        {
            context = _context;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Cliente>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public IActionResult get()
        {
            try
            {

                var lista = context.Cliente.ToList();

                if (lista.Count == 0)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "No se encontró información.");
                }
                else
                {
                    return StatusCode(StatusCodes.Status200OK, lista);
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error: " + ex.Message);
            }
        }

        [HttpGet]
        [Route("getById/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Cliente))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public IActionResult get(int id)
        {
            try
            {

                var lista = context.Cliente.Find(id);

                if (lista == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "No se encontró información.");
                }
                else
                {
                    return StatusCode(StatusCodes.Status200OK, lista);
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error: " + ex.Message);
            }
        }


        [HttpGet]
        [Route("getByNombre/{nombre}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Cliente>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public IActionResult get(string nombre)
        {
            try
            {

                var lista = context.Cliente.Where(x => x.Nombre.StartsWith(nombre)).ToList();

                if (lista.Count == 0)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "No se encontró información.");
                }
                else
                {
                    return StatusCode(StatusCodes.Status200OK, lista);
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error: " + ex.Message);
            }
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Cliente))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public IActionResult Post(Cliente model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    context.Add(model);
                    context.SaveChanges();
                    return StatusCode(StatusCodes.Status201Created, model);
                }
                else
                {
                    string mensajes = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                    return StatusCode(StatusCodes.Status500InternalServerError, mensajes);
                }
            }catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error: " + ex.Message);
            }
        }


        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Cliente))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public IActionResult Put(Cliente model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if(context.Cliente.Where(x => x.IdCliente == model.IdCliente).Any())
                    {
                        context.Update(model);
                        context.SaveChanges();
                        return StatusCode(StatusCodes.Status200OK, model);
                    }
                    else
                    {
                        return StatusCode(StatusCodes.Status404NotFound, "No se encontró la información del cliente.");
                    }                    
                }
                else
                {
                    string mensajes = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                    return StatusCode(StatusCodes.Status500InternalServerError, mensajes);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error: " + ex.Message);
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Cliente))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public IActionResult Delete(int id)
        {
            try
            {
                var objeto = context.Cliente.Find(id);
                if (objeto != null)
                {
                    context.Cliente.Remove(objeto); 
                    context.SaveChanges();
                    return StatusCode(StatusCodes.Status200OK, objeto);
                }
                else
                {
                    return StatusCode(StatusCodes.Status404NotFound, "No se encontró la información del cliente.");
                }
              
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error: " + ex.Message);
            }
        }

    }
}
